'use strict';

const gulp = require('gulp'),
    { watch } = gulp,
      sass = require('gulp-sass'),
      sourcemaps = require('gulp-sourcemaps');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
    return gulp.src('./scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./'));
} );

gulp.task( 'sass:watch', function () {
    watch(['./scss/**/*.scss'], gulp.series('sass'))
} )